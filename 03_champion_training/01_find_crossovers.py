#!/usr/bin/env python
"""
Determines the crossover between given champion folders.
Can help remove from the combined no folder.

All numbers are < 1000
"""
import re
import glob
from pprint import  pprint

EXTENSION = ".jpg"

def determine_root(inp: str):
    """Determines the youtube id of the given file"""
    match = ""
    reg = [r"(.*)\d\d\d.jpg", r"(.*)\d\d.jpg", r"(.*)\d.jpg"]
    try:
        match = re.match(reg[0], inp)
        if match is None:
            raise IndexError
    except IndexError:
        try:
            match = re.match(reg[1],inp)
            if match is None:
                raise IndexError
        except IndexError:
            try:
                match = re.match(reg[2], inp)
            except:
                return -1 # This is not an image
    return match.group(1)

def cross_over(master: str, secondary: str):
    """
    Checks what crossovers exists between the master and secondary
    Returns a list of matches
    """
    print ("Master: ", master, " and secondary: ", secondary)

    master_files = [determine_root(x) for x in glob.glob(master + "/*" + EXTENSION) if determine_root(x) != -1]
    secondary_files = [determine_root(x) for x in glob.glob(secondary + "/*" + EXTENSION) if determine_root(x) != -1]
 
    master_files = list(dict.fromkeys(master_files))
    secondary_files = list(dict.fromkeys(secondary_files))

    master_files = [x.split("/")[-1] for x in master_files]
    secondary_files = [x.split("/")[-1] for x in secondary_files]

    print ("Master files...")
    pprint(master_files)
    print ("Secondary files...")
    pprint(secondary_files)

    return [x for x in master_files if x in secondary_files]

if __name__ == "__main__":
    import sys
    if len(sys.argv) < 3:
        pprint ("Usage: [master folder] [slave folder]")
    else:
        pprint(cross_over(sys.argv[1], sys.argv[2]))
