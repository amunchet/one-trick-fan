#!/usr/bin/env python
"""
Main runner for a major champion training
"""
import sys
import simple
import os
import glob
import threading
import numpy as np
import shutil
import multiprocessing

sys.path.append("../01_simple_training")

scrape = __import__("01_scrape")
download = __import__("02_download")
resize = __import__("03_resize")

THREAD_COUNT = multiprocessing.cpu_count() - 2 

def main(champions: str, pages: int, skip_download = False):
    """Main function"""

    if not os.path.exists("./champions"):
        os.mkdir("champions")

    champ = "champions/" + champions

    print("Starting for ", champ)

    if not os.path.exists("./" + champ):
        print ("Creating directory for ", champ)
        os.mkdir("./" + champ)

    os.chdir("./" + champ)


    # Step 1: Scrape the given champs from LoG
    if not skip_download:
        print ("Starting scrape...")
        found_urls = scrape.download_champ(champions, pages)
        
        print ("Starting URLs...")
        threads = []
        for url in found_urls:
            print("Starting video processing for ", url)
            fname = download.download_video(url)

            print ("Starting thread for ffmpeg")
            t = threading.Thread(target = download.run_ffmpeg , args=(fname, True,))
            threads.append(t)
            t.start()

        for thread in threads:
            thread.join()

        
        # Create yes and no folders if they don't exist
        if not os.path.exists("./yes"):
            os.mkdir("./yes")

        if not os.path.exists("./no"):
            os.mkdir("./no")

    #secondary_check()

    # Step 4: Cleanup
    os.system("rm -- *.png")
    os.system("rm -- *.mkv")
    os.chdir("../")


    print ("Completed.")

def secondary_check():
    def inside_check(fname):
        print("Resizing ", fname)
        # Step 2: Resize to FULL SIZED (long) images
        new_fname = fname.replace(".png", ".jpg")
        resize.resize(fname, new_fname)

        # Step 3: Determine whether or not a good image
        if(simple.simple_eval(new_fname)):
            print("Yes!")
            shutil.copy2(new_fname, "./yes/")
        else:
            print("No")
            shutil.copy2(new_fname, "./no/")

    file_list = glob.glob("*.png")
    file_list_yes = [x.replace(".jpg", ".png").split("/")[-1] for x in glob.glob("yes/*.jpg")]
    file_list_no = [x.replace(".jpg", ".png").split("/")[-1] for x in glob.glob("no/*.jpg")]

    print ("Length of files: ", len(file_list))
    print("Length of no: ", len(file_list_no))
    print("Length of yes: ", len(file_list_yes))

    print ("Top of yes: ", file_list_yes[:3])
    print ("Top of no: ", file_list_no[:3])
    print ("Top of full: ", file_list[:3])

    file_list = [x for x in file_list if x not in file_list_yes and x not in file_list_no]

    print("Updated file list length: ", len(file_list))

    for fgroup in np.array_split(file_list, int(len(file_list)/THREAD_COUNT)):
        threads = []
        print("Fgroup: ", fgroup)
        for fname in fgroup:
            print("Fname: ", fname)
            t = threading.Thread(target = inside_check, args=(fname,))
            threads.append(t)
            t.start()

        for thread in threads:
            thread.join()
        
if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Usage: [champion name] [number of pages] [skip download = False]")
    else:
        if len(sys.argv) == 3:
            main(sys.argv[1], int(sys.argv[2]))
        else:
            print ("Starting with download skip")
            main(sys.argv[1], int(sys.argv[2]), True)
