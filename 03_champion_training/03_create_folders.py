#!/usr/bin/env python
"""
Creates the Train and Validate folders, given a champion to train

## DATA ASSEMBLY STEPS
This is what to do when training a new champion classifier.

1.  Create Folder called Train
2.  Copy all the yes from the champion to Train/yes
3.  Copy a matching number of yes from other champions to Train/no
4.  Check for overlap with `01_find_crossovers.py [file1] [file2]`
5.  Remove any files from Train/no - VERY IMPORTANT
6.  Run `02_production_resize [folder]` on both Train/yes and Train/no
    6a. [Optional - can remove the 0 size files here]
7.  Create folder called Valid
8.  Bring over a few files from Train/yes (MOVE not copy)
9.  Bring over a few files from Train/no (MOVE not copy)
10. Copy over `Train/` and `Valid/` folders to the server for training


"""
import glob
import sys
import os
import shutil
import random

crossovers = __import__("01_find_crossovers")
resize = __import__("02_production_resize")

def create_train_validate(champ_name: str):
    """Does the Data assembly"""
    print("Starting up...")
    for fname in ["Train", "Valid"]:
        if os.path.exists(fname):
            shutil.rmtree(fname)
        os.mkdir(fname)
        os.mkdir(fname + "/yes")
        os.mkdir(fname + "/no")

    print("Folders created")

    for fname in glob.glob(champ_name + "/yes/*"):
        shutil.copy(fname, "Train/yes")

    # Populate champion images
    num_yes = len(glob.glob(champ_name + "/yes/*"))
    print(num_yes, " number of champion images")

    for fname in [x for x in glob.glob("*/yes/*") if "Train" not in x and "Valid" not in x][:num_yes]:
        shutil.copy(fname, "Train/no")
    
    num_no = len(glob.glob(champ_name + "/no/*"))
    print(num_no, " number of anti-champion images")
    
    # Find crossovers
    found_collisions = crossovers.cross_over("Train/yes", "Train/no")
    print(found_collisions)

    for coll in found_collisions:
        for fname in glob.glob("Train/no/" + coll + "*"):
            os.remove(fname)

    print("Removed collisions...")
    found_collisions = crossovers.cross_over("Train/yes", "Train/no")
    print(found_collisions)
    
    if found_collisions != []:
        raise Exception("Collisions still exist between Training folders")
    
    # Production resizing
    resize.folder_resize("Train/yes")
    resize.folder_resize("Train/no")

    # Remove 0 files
    print ("Removing 0 files...")
    os.system("cd Train/yes && (ls -l | awk ' { if($5 == \"0\"){ system(\"rm \" $9);}}') && cd ../..") 
    os.system("cd Train/no && (ls -l | awk ' { if($5 == \"0\"){ system(\"rm \" $9);}}') && cd ../..") 
    
    # Randomly MOVE files from Train/yes,no to Valid/yes,no
    print ("Creating the Validation folders")
    for dirs in ["yes", "no"]:
        for fname in glob.glob("Train/" + dirs + "/*"):
            if(random.random() > .95):
                shutil.move(fname, "Valid/" + dirs + "/" + fname.split("/")[-1])

    valid_yes = len(glob.glob("Valid/yes/*"))
    valid_no = len(glob.glob("Valid/no/*"))

    print("Valid yes and no: ", valid_yes, valid_no)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print ("Usage: [champion name]")
    else:
        create_train_validate(sys.argv[1])
