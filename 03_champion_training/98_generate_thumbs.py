#!/usr/bin/env python
"""Generates a HTML page with thumbnails"""
import glob
import sys

def main(fname):
    """Runs vs the given fname"""
    header = """
    <html>
    <head>
    <style>
    *{
    margin: 0;
    padding: 0;
    }
    div.outer{
        width: 100%;
    }
    div.inner{
        width: 33%;
        float: left;
    }
    div.inner img{
        width: 100%;
    }
    </style>
    <script type='text/javascript'>
    </script>
    </head>
    <body>
    <div class='outer'>
    """
    for item in sorted(glob.glob(fname + "/*.jpg")):
        header += "<div class='inner'><h2>" + item.split("/")[-1] + "</h2><img src='" + item.split("/")[-1] + "' /></div>"

    header += "</div></body></html>"
    print(header)

if __name__ == "__main__":
    main(sys.argv[1])
