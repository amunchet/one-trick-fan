# Training folder
---
Due to the failure of the templating in OpenCV, we are going to create training data for finding a champion (in this case, Viktor) in the images. 

Resize needs to create a constant size.  If we need to scale, we can scale

# Helpful script
- To remove any 0 size files, use the following `ls -l | awk ' { if ($5 == "0"){ system("rm " $9);}}'`

# NEW PLAN
- Going to receive input from the simple_training to be able to tell when the champions are visible on the screen and when they are not.
- From there, going to download and process the League of Graphs videos for the given champs



## Data Collection Plan
- Going to scrape League of Graphs 
	- scrape.py
	- Download to urls.txt
	- `youtube-dl` $urls.txt
	- `ffmpeg -i [FILENAME] -vf fps=1 out%d.png`
- Probably 10 pages per champion
- Going to download the VOD
- Can reasonably turn minute 20 - 40 into thumbnails
- From there, want to create a playground app that goes through them and assigns them to either "yes" or "no" folders.

- We will then find some non viktor games and send them entirely to the no folders to balance.

## ML Plan
- Validation and Training set splits
- From here, going to pretrain on imageNet
- Then it's a simple binary classifier - is Viktor here or not?


## Other Notes
- We are using relative imports to reuse the 01_simple_training code
- We also copied over the export.pkl file, but have it in gitignore

## DATA ASSEMBLY STEPS
This is what to do when training a new champion classifier.

1.  Create Folder called Train
2.  Copy all the yes from the champion to Train/yes
3.  Copy a matching number of yes from other champions to Train/no
4.  Check for overlap with `01_find_crossovers.py [file1] [file2]` 
5.  Remove any files from Train/no - VERY IMPORTANT
6.  Run `02_production_resize [folder]` on both Train/yes and Train/no

[Optional - can remove the 0 size files here]

7.  Create folder called Valid 
8.  Bring over a few files from Train/yes (MOVE not copy)
9.  Bring over a few files from Train/no (MOVE not copy)
10. Copy over `Train/` and `Valid/` folders to the server for training

## NVIDIA NOTES
- Ended up having to install a bunch of things in the NVIDIA runtime - updated the drivers to 440 as well.
- Most important were the apt get libencode and libdecode.  Other packages were required as well
- libnvcuvid.so.1 and libnvcuvid.so are the files that need to be installed via apt
