#!/usr/bin/env python
"""
Resize function to trim down the input stream image to only champions
PRODUCTION VERSION - Simple version was trained without support
"""
import cv2
import numpy as np
import threading
import glob
import multiprocessing

size = 250
THREAD_COUNT = multiprocessing.cpu_count() - 2

def production_resize(inp, outp):
    """Fixes the resize to proper aspect ratio"""
    src = cv2.imread(inp)

    new_start = size-100
    new_end = src.shape[1] + size

    b = src[new_start:new_end,:,0]

    b = cv2.resize(b, (new_end - new_start, new_end - new_start))
    cv2.imwrite(outp, b)

def folder_resize(folder: str):
    """Runs production resize in threads for a given folder"""
    file_list = glob.glob(folder + "/*.jpg")
    for fgroup in np.array_split(file_list, int(len(file_list)/THREAD_COUNT)):
        threads = []
        for fname in fgroup:
            print("Fname: ", fname)
            t = threading.Thread(target = production_resize, args=(fname, fname,))
            threads.append(t)
            t.start()

        for thread in threads:
            thread.join()

if __name__ == "__main__":
    import sys
    if len(sys.argv) < 2:
        print ("Usage: [folder]")
    else:
        folder_resize(sys.argv[1])
