#!/usr/bin/env python3
import sys
import numpy as np
import matplotlib.pyplot as plt
import glob
import cv2
import shutil
import os

fnames = sorted(glob.glob("*.bmp"))


if not os.path.exists("./no"):
    os.mkdir("./no")

print ("Clear of no")
if not os.path.exists("./yes"):
    os.mkdir("./yes")
print ("Clear of yes")


def press(event):
    print('press', event.key)
    sys.stdout.flush()

    if event.key == 'y' or event.key == 'n':
        #ax.plot(np.random.rand(12), np.random.rand(12), 'go')
        if len(fnames) > 0:
            name = fnames.pop()
            ax.imshow(cv2.imread(name))
            ax.set_title(name)
            fig.canvas.draw()

            if event.key == 'y':
                shutil.move(name, "./yes")
            else:
                shutil.move(name, "./no")

print("Clear random")

fig, ax = plt.subplots()

print("Clear subplot")
fig.canvas.mpl_connect('key_press_event', press)

print ("Clear others")
xl = ax.set_xlabel('Press y to approve, n to reject')
ax.set_title('---')
plt.show()
