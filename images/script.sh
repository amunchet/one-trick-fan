#!/bin/sh
# Script to download the images from the two pages of
# https://lol.gamepedia.com/Category:Champion_Square_Images
# and
# https://lol.gamepedia.com/index.php?title=Category:Champion_Square_Images&filefrom=YorickSquare.png#mw-category-media

cat index2.html | grep img | grep Square.png | awk -F"Square.png" ' { print $3 "Square.png" $4 } ' | awk -F ".png" ' { print $1 ".png" } ' | sed 's/.*src=\"//g' > urls2.txt

awk ' { system("wget " $0) } ' urls2.txt
