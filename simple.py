#!/usr/bin/env python
"""Performs the evaluation of a given image"""
import fastai
from fastai.imports import *
from fastai.vision import *

PICKLE_FILE="simple.pkl"

learn = load_learner("models", file=PICKLE_FILE)

def simple_eval(fname: str):
    """
    Function to use the simple model to determine if image shows champion icons
        - fname: Filename to evaluation
        - Returns True or False 
    """
    found = str(learn.predict(open_image(fname))[0])
    if found == "yes":
        return True
    elif found == "no":
        return False
    else:
        raise Exception("Invalid type found in evaluation")


