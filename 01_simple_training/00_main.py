#!/usr/bin/env python
"""
Main script to run simple training data collector
    - Once completed, want to message to run the manual comparator
"""
import glob
import time
import os
scrape = __import__("01_scrape")
download = __import__("02_download")
resize = __import__("03_resize")

def main():
    """
    Main run
    """
    start = time.time()
    print("Starting up...")
    
    champs = ["thresh", "yasuo", "syndra", "sona", "swain"]
    for champ in champs:
        champ_start = time.time()
        if not os.path.exists("./" + champ):
            os.mkdir("./" + champ)

        print("Current directory: ", os.getcwd())
        os.chdir("./" + champ)
        print ("New directory: ",  os.getcwd())
        

        found_urls = scrape.download_champ(champ, 1)
        print ("Finished downloading URLs...")
        for url in found_urls:
            print("Starting video processing for ", url)
            fname = download.download_video(url)
            download.run_ffmpeg(fname + ".mkv", True)
            print ("Completed.")

        print ("Starting up resizing")
        for fname in glob.glob("*.png"):
            print ("Starting resize for ", fname)
            resize.resize(fname, fname.replace("png", "jpg"))

    
        # Clean up unneeded files
        os.system("rm *.png")
        os.system("rm *.mkv")


        print("Current direcotry: ", os.getcwd())
        os.chdir("../")
        print("Changed back to ", os.getcwd())

        champ_end = time.time()
        print(champ + " processed in ", champ_end - champ_start, " seconds")

    print("Done.")
    end = time.time()
    print (end-start, " seconds elapsed")

if __name__ == "__main__":
    main()

