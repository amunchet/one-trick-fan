#!/usr/bin/env python
"""
Compares the given folder to folder-yes and creates a folder-no with the omitted files in it.
"""
import glob
import os
import shutil

def create_no(infile: str):
    """
    Creates the [CHAMP NAME]_no folder, given that both the [CHAMP NAME] folder and the [CHAMP NAME]_yes folders already exist
    """
    yes_arr = [x.replace("-yes", "") for x in glob.glob(infile + "-yes/*") ]

    no_arr = [x for x in glob.glob(infile + "/*") if x not in yes_arr]

    print ("Moving to no folder")

    if not os.path.exists(infile + "-no"):
        print("Created ", infile + "-no")
        os.mkdir(infile + "-no")


    for fname in no_arr:
        print("Copying for ", infile)
        shutil.copy2(fname, infile + "-no")
    
    print("Completed")
