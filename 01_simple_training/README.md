# Summary
Simple training system.  The point of this is to simply determine whether or not the given image is showing an in-game shot of the champions or a cut scene/other non-important part.

# Steps
1.  Download the given youtube videos from a list (urls.txt)
2.  Convert to images.
3.  Resize given images.  We'll do the proper aspect ratio splitting later
4.  Pass along to the sorting.  

Outputting the final training folders as "yes" and "no"

## General Thoughts

- We probably want to have a wide variety, instead of many per file.
- Each video is 1 hour - 60 minutes - 3600 seconds.  3600 images with maybe 60% as matches.  
- If we do 10 videos, we should have about 36000 images - should be good for the simple classifier.  



## Sorting
1.  Copying the folder to yes (e.g. "viktor" is copied to "viktor-yes"
2.  Run the comparative script (05), which will create "viktor-no"

## Additional Thoughts
- Do we want to do the artificial data?  Probably only if we get super bored.  Using actual videos will provide better training data anyways.  
