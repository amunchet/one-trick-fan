#!/usr/bin/env python
"""Downloads the given list of URLs and converts to images."""
import youtube_dl
import os

def download_video(url):
    """
    Downloads the given video
    """
    ydl = youtube_dl.YoutubeDL({'outtmpl': '%(id)s'})

    with ydl:
        result = ydl.extract_info(
            url
        )

    if 'entries' in result:
        # Can be a playlist or a list of videos
        video = result['entries'][0]
    else:
        # Just a video
        video = result

    video_id = video['id']
    print(video_id)
    return video_id 

def run_ffmpeg(fname, experimental=False):
    """
    Stub function to run the ffmpeg and convert to actual images
        - Want to consider collisions with same named files
    """
    if not experimental:
        os.system("ffmpeg -i " + fname + " -vf fps=1/10 " + fname.split('.')[0] + "%d.png")
    else:
        a = """docker run --rm --runtime=nvidia \
            --volume $PWD:/workspace \
            jrottenberg/ffmpeg:4.1-nvidia \
            -hwaccel cuvid \
            -c:v h264_cuvid \
            -i /workspace/input.mp4 \
            -vf fps=1/10 \
            -c:v hevc_nvenc \
            /workspace/out%d.png 
        """
        # replace("input.mp4", fname).replace("out", fname.split('.')[0])

        a = """docker run --rm --runtime=nvidia --volume $PWD:/workspace jrottenberg/ffmpeg:4.1-nvidia -nostats -loglevel 0 -hwaccel cuvid -c:v h264_cuvid  -i /workspace/hmNDexvlpr8 -vf "scale_npp=format=yuv420p,hwdownload,format=yuv420p,fps=1/10" -pix_fmt yuvj420p -color_range 2  -y /workspace/out%d.jpg""".replace("hmNDexvlpr8", fname).replace("out", fname.split('.')[0])

        os.system(a )
    return True


