#!/usr/bin/env python
"""
Resize function to trim down the input stream image to only champions
"""
import cv2
import numpy as np

size = 125

def resize(inp, outp):
    """
    Takes input and saves to output file
        - Takes only the left most 10% and right most 10% of the image and combines
        - Eventually, this will need to be split into squares and scaled to 224x224
        - Unnecessary for the simple training, since that split will be done later, after sorting
    """
    src = cv2.imread(inp)
    start = int(src.shape[1] * .1)
    end = int(src.shape[1] * .9)

    # This was really slow previously
    """
    for x in range(start, end):
        src = np.delete(src, start, axis=1)
    b = src[:,:, 0]
    """

    # Changed from [:,:,0]
    #b = src[size:src.shape[1]+size,:,0]

    b = src[:,:start,0]
    b = np.c_[b, src[:, end:, 0]]

    cv2.imwrite(outp, b)

def fix_resize(inp, outp):
    """Fixes the resize to proper aspect ratio"""
    src = cv2.imread(inp)
    b = src[size:src.shape[1]+size,:,0]
    cv2.imwrite(outp, b)

if __name__ == "__main__":
    resize("out1.bmp", "out2.png")
