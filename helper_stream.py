#!/usr/bin/env python
"""Used to download and convert stream to images"""

from streamlink import Streamlink
import os

# Use a dotenv file for the list of streams
FPS = 5

def download_stream(url: str):
    """Downloads the specified stream"""
    # url = 'https://www.twitch.tv/lec'

    session = Streamlink()
    session.set_option("http-timeout", 5)
    session.set_option("http-stream-timeout", 5)
    streams = session.streams(url)
    if len(streams) == 0:
        return False

    stream = streams["best"]
    fd = stream.open()
    data = fd.read(1024000 * 100)
    with open('output.ts', 'wb') as f:
        f.write(data)


    os.system("ffmpeg -i output.ts -vf fps=" + str(FPS) + " out%d.bmp")



