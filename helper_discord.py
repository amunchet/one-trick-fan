#!/usr/bin/env python
"""
Discord bot control center
    - One of the main things is that this bot doesn't have to be running 24/7
    - We can start it up when it's time to say something, then turn off
"""

def bot_init():
    pass

def bot_create_channel():
    pass

def bot_broadcast():
    pass


