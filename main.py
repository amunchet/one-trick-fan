#!/usr/bin/env python
"""
Main program
    - One thing we need to do is keep track of when a game ends and another begins.
        + This should be done via counting the number of times the simple network says "no", but the stream is still running.

"""
import sys
import glob
import os
from helper_stream import download_stream
import fastai
from fastai.imports import *
from fastai.vision import *
import simple

sys.path.append("01_simple_training")
sys.path.append("03_champion_training")

resize = __import__("03_resize")
production_resize = __import__("02_production_resize")


def main(url: str, champion: str):
    """Main function"""

    # Step 0: Load the proper champion check
    learn = load_learner("models", file=champion + ".pkl")
    learn.to_fp32()
    # Step 1: Download the Stream
    #print ("Beginning Download...")
    a = download_stream(sys.argv[1])
    if a == False:
        print("Stream Offline")
        return False
 
    # Step 2: Resize the out*.bmp
    for fname in glob.glob("out*.bmp"):
        #print ("Looking at ", fname)
        new_fname = fname.replace(".bmp", ".png")
        resize.resize(fname, new_fname)
        
        #print("Starting production resize...")
        production_resize.production_resize(new_fname, new_fname)
        # Step 3: Evaluate if we have an actual check
        if(simple.simple_eval(new_fname)):
            # Step 4: Run the champion tester
            print("We have champions! ", new_fname)
            
            full_predict = learn.predict(open_image(new_fname))

            if str(full_predict[0]) == "yes":
                print ("We saw ", champion, " in ", new_fname)
            else:
                print ("We did not see ", champion)

        else:
            # Record that we are in no champions at the moment
            print ("No champion photos seen")

        # Step 5: Cleanup
        os.remove(fname)
        os.remove(new_fname)


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print ("Usage: [stream url] [champion]")
    else:
        main(sys.argv[1], sys.argv[2])
    
