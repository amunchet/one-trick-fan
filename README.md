# One Trick Fan
## Summary

Discussed in depth at [http://www.chetenright.com/blog](http://www.chetenright.com/blog)

Package to a) develop neural networks to recognize league of legends champions, b) watch professional LoL streams, and c) communicate through Discord when a particular champion is being played professionally.

## Required Binary Packages to be installed separately
- ffmpeg



## Historical Notes
- Start up virtual env (pyhton3 -m venv)
- images contains all champion images (and names)
- Using OpenCV templates was a failure, even with a large number of keypoints.
